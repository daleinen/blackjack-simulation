Blackjack-Simulation
====================

A blackjack program to simulate millions of hands, games, and sessions.

NOTE: Since this is a blackjack simulation program there is not need for suits.  In fact, no
traditional blackajck game really needs suits.

I am not an advanced programmer.  I just code what
I know, but I love getting stuck for hours and then BOOM! I get it.
Feel free to let me know what can be improved.

**Overview**

The purpose of this program is to simulate hundreads of thousands of sessions of the
game of blackjack using a certain betting system.  Sessions is the equilvilant of one evening 
of blackjack at a casino.  In real life you go to the casino and either you win some money or leave
empty handed.  At least I do anyway.  I wrote this program to simulate those nights many many times
and see if the style of play I have is of any consequence.  I know any statistician worth his salt
will tell you that betting systems are worthless but I want to be sure because the system I use you 
won't find in any book, at least not that I have found.  I use a variation of the martingale system
and a very simple card count.  In theory, I would double my bet after each loss unless either the amount
my winnings or card count told me not to.

{UPDATES:}

10OCT13 - Dealer strategy, splits, and comparing hands are done.  I know that the splits 
are a real mess.  I will eventually create a singleton for the deck and create methods so 
there won't be so much redundancy.  Still need to get the statistics completed.  There are a lot
of println statements which can and should be commented out for high number sessions.

30SEP13 - About half of the way there. Right now I still need to work on the splits,
dealer strategy, statistics, card count, and comapring hands.