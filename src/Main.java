/**
 * 
 * @author daleinen
 * @since 9/18/13
 * The main class of program
 *
 */
import java.util.ArrayList;
import java.util.Scanner;
public class Main {
	
	private static double my_stack;
	private static double bet;
	private static int deck_number;
	private static int table_min;
	private static int wins;
	private static int losses;
	private static int total_doubles;
	private static int double_wins;
	private static int pushes;
	private static int hands;
	private static int shoes;
	private static int blackjacks;
	private static int sessions;
	private static int sessions_wins;
	private static int sessions_losses;
	private static int starting_amount;
	private static int stop_amount;
	
	private static int player_aces = 0;
	private static int dealer_aces = 0;
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner (System.in);
		
		//get user input for table minimum and error check
		System.out.println("Please enter the table min($5-$10-$15-$25): ");
		table_min = scanner.nextInt();
		while(table_min != 5 && 
				table_min != 10 && 
					table_min != 15 && 
						table_min != 25){
			System.out.println("Please enter a valid table min($5-$10-$15-$25): ");
			table_min = scanner.nextInt();
		}
		
		System.out.println("Please enter stop playing amount(x2-x4-x6): ");
		stop_amount = scanner.nextInt();
		while (stop_amount != 2 &&
					stop_amount != 4 &&
						stop_amount != 6){
			System.out.println("Please enter a valid stop playing amount(x2-x4-x6): ");
			stop_amount = scanner.nextInt();
		}
		
		//get user input for number of decks and error check
		System.out.println("Please enter the number of decks(1-2-4-6-8): ");
		deck_number = scanner.nextInt();
		while(deck_number != 1 &&
				deck_number != 2 && 
					deck_number != 4 &&
						deck_number != 6 && 
							deck_number != 8 ){
			System.out.println("Please enter a valid number of decks(1-2-4-6-8): ");
			deck_number = scanner.nextInt();
		}
		
		//gets user input for sessions
		System.out.println("Please enter number of sessions: ");
		sessions = scanner.nextInt();
		
		scanner.close();
		
		//setting starting amount equal to an amount suitable for table min
		switch (table_min){
		case 5:
			starting_amount = 155;
			break;
		case 10:
			starting_amount = 310;
			break;
		case 15:
			starting_amount = 225;
			break;
		case 25:
			starting_amount = 375;
			break;
		}
		stop_amount = stop_amount * starting_amount;
		bet = table_min;
		
		System.out.println();

		
		while (sessions > 0){
			
			playBJ();
			System.out.println("----------S-----------");
			sessions--;
		}
		
		
	
	}
	
	//the actual black jack game(
	public static void playBJ(){
		
		Deck cards = new Deck();
		Strategy strat = new Strategy();
		
		my_stack = starting_amount;
		//bet = 0;
		hands = 0;
		cards.gameDeck(deck_number);
		cards.deckShuffle(); 
		
		while (my_stack >= bet && my_stack < stop_amount){
			
			ArrayList<Integer> playerList = new ArrayList<Integer>();
			ArrayList<Integer> dealerList = new ArrayList<Integer>();
		
			ArrayList<Integer> split_1 = new ArrayList<Integer>();
			ArrayList<Integer> split_2 = new ArrayList<Integer>();
			ArrayList<Integer> split_3 = new ArrayList<Integer>();
			ArrayList<Integer> split_4 = new ArrayList<Integer>();
		
			boolean dealer_hit = true;
			boolean player_bust = false;
			boolean dealer_bust = false;
			boolean second_split = false;
			boolean third_split = false;
			boolean playerBJ = false;
			boolean dealerBJ = false;
			boolean in_hand = false;
			int my_doubles = 0;
			int my_splits = 0;
			int split_wins = 0;
			int split_losses = 0;
			int player_card_1;
			int player_card_2;
			int dealer_card_1;
			int dealer_card_2;
			int player_value = 0;
			int dealer_value = 0;
			//for splits
			int hand_1 = 0;
			int hand_2 = 0;
			int hand_3 = 0;
			int hand_4 = 0;
			
			player_aces = 0;
			dealer_aces = 0;
			
			//cards.printDeck(52 * deck_number);
			
			//dealing two cards for both player and dealer
			//the start of each hand
			player_card_1 = cards.dealCard(in_hand);
			if (aceCheck(player_card_1)) player_aces++;
			player_value = getPlayerValue(player_card_1, player_value, player_aces);
			playerList.add(player_card_1);
			
			dealer_card_1 = cards.dealCard(in_hand);
			if (aceCheck(dealer_card_1)) dealer_aces++;
			dealer_value = getPlayerValue(dealer_card_1, dealer_value, dealer_aces);
			dealerList.add(dealer_card_1);
			
			player_card_2 = cards.dealCard(in_hand);
			if (aceCheck(player_card_2)) player_aces++;
			player_value = getPlayerValue(player_card_2, player_value, player_aces);
			playerList.add(player_card_2);
			
			dealer_card_2 = cards.dealCard(in_hand);
			if (aceCheck(dealer_card_2)) dealer_aces++;
			dealer_value = getPlayerValue(dealer_card_2, dealer_value, dealer_aces);
			dealerList.add(dealer_card_2);
			
			System.out.println("***Hand " + hands + "***");
			System.out.println("player cards - " + playerList);
			System.out.println("dealer cards - " + dealerList);
			System.out.println("Player aces: " + player_aces + " Dealer aces: " + dealer_aces);
			System.out.println("Dealer up: " + dealer_card_1);
			System.out.println("Player-->" + player_value);
			System.out.println("Dealer-->" + dealer_value);
			System.out.println();
			
			/**
			 * 
			 * HERE WE START CHECKING FOR BLACKJACKS,
			 * SPLITS, DOUBLES, ETC.
			 * 
			 */
			while(true){
				//so we don't shuffle in the middle of play
				in_hand = true;
				
				//checking for blackjack
				if (player_value == 21 && dealer_value == 21){
					break;
				}
				
				if (player_value == 21){
					dealer_hit = false;
					playerBJ = true;
					System.out.println("My BJ!");
					break;
				}
				
				else if (dealer_value == 21){
					dealerBJ = true;
					System.out.println("Dealer BJ!");
					break;
				}
				
				/**
				 * 
				 * THE SPLIT CHECK, IT IS VERY CONFUSING I REALIZE CONSIDERING
				 * THAT MOST CASINOS LET YOU SPLIT 3 TIMES MAKING 4 SEPERATE
				 * HANDS WHICH IS WHAT I SIMULATED HERE.
				 * I WILL TRY TO GET REPEATING CODE INTO A METHOD LATER!
				 * 
				 */
				else if(strat.splitCheck(player_card_1, player_card_2, 
							dealer_card_1, deck_number) == true){
					
					System.out.println("Split it!");
					
					my_splits++;
					
					/**
					 * THIS CHECKS FOR SPLITING ACES ONLY
					 * AS YOU GET ONE CARD ONLY FOR EACH HAND
					 */
					if (player_card_1 == 1 && player_card_2 == 1){
						
						if (player_card_1 == 1) {
							player_card_1 = 11;
							player_card_2 = 11;
						}
						int extra_1 = cards.dealCard(in_hand);
						int extra_2 = cards.dealCard(in_hand);
						split_1.add(player_card_1);
						split_2.add(player_card_2);
						split_1.add(extra_1);
						split_2.add(extra_2);
						hand_1 = getPlayerValue(extra_1, player_card_1, player_aces);
						hand_2 = getPlayerValue(extra_2, player_card_2, player_aces);
						
						System.out.println(split_1);
						System.out.println(hand_1);
						
						if (hand_1 == 21){
							System.out.println("21 on split!");
						}
						System.out.println("----------");
						System.out.println(split_2);
						System.out.println(hand_2);
						
						if (hand_2 == 21){
							System.out.println("21 on split!");
						}
						
						break;
					}
					
					
					/**
					 * HAND 01 very first card and starting hand
					 */
					int extra_1 = cards.dealCard(in_hand);
					if (aceCheck(extra_1) == true) player_aces++;
					
					//checking for a second/third split
					if (extra_1 == player_card_1){
						my_splits++;
						second_split = true;
						System.out.println("First extra: " + extra_1);
						extra_1 = cards.dealCard(in_hand);
						if (aceCheck(extra_1) == true) player_aces++;
						System.out.println("Second split!");
						
						if (extra_1 == player_card_1){
							my_splits++;
							third_split = true;
							System.out.println("Second extra: " + extra_1);
							extra_1 = cards.dealCard(in_hand);
							if (aceCheck(extra_1) == true) player_aces++;
							System.out.println("Third split!");
							
						}
						
					}
					

					hand_1 = player_card_1;
					
					split_1.add(player_card_1);
					split_1.add(extra_1);
					
					//System.out.println(extra_1 + " " + hand_1 + " " + player_aces);
					hand_1 = getPlayerValue(extra_1, hand_1, player_aces);
					System.out.println(split_1);
					System.out.println(hand_1);
					
					if (hand_1 == 21){
						System.out.println("21 on split!");
					}
					
					//checking hand01
					HAND01LOOP:
					while (true){
						if (strat.doubleCheck(hand_1, player_aces, dealer_card_1, deck_number) == true){
							System.out.println("Double it on split!");
							int extra_card;
							extra_card = cards.dealCard(in_hand);
							hand_1 = getPlayerValue(extra_card, hand_1, player_aces);
							split_1.add(extra_card);
							System.out.println(extra_card + "--" + hand_1);
							break;
						}
						while (strat.hitCheck(hand_1, dealer_card_1, player_aces) == true){
							System.out.println("Hit it on a split!");
							int extra_card;
							extra_card = cards.dealCard(in_hand);
							if (aceCheck(extra_card) == true) player_aces++;
							hand_1 = getPlayerValue(extra_card, hand_1, player_aces);
							split_1.add(extra_card);
							System.out.println(extra_card + "--" + hand_1);
						}
						
						break;
					}
					
					System.out.println("-----------");
					
					/**
					 * 
					 * HAND 02
					 * 
					 */
					int extra_2 = cards.dealCard(in_hand);
					if (aceCheck(extra_2) == true) player_aces++;
					
					//checking for a second/third split
					if (extra_2 == player_card_2){
						my_splits++;
						second_split = true;
						System.out.println("First extra: " + extra_2);
						extra_2 = cards.dealCard(in_hand);
						if (aceCheck(extra_2) == true) player_aces++;
						System.out.println("Second split!");
						
						if (extra_2 == player_card_1){
							my_splits++;
							third_split = true;
							System.out.println("Second extra: " + extra_2);
							extra_2 = cards.dealCard(in_hand);
							if (aceCheck(extra_2) == true) player_aces++;
							System.out.println("Third split!");
							
						}
						
					}

					hand_2 = player_card_2;
					
					split_2.add(player_card_2);
					split_2.add(extra_2);
					
					//System.out.println(extra_2 + " " + hand_2 + " " + player_aces);
					hand_2 = getPlayerValue(extra_2, hand_2, player_aces);
					System.out.println(split_2);
					System.out.println(hand_2);
					
					if (hand_2 == 21){
						System.out.println("21 on split!");
					}
					
					//checking hand02
					HAND02LOOP:
					while (true){
						if (strat.doubleCheck(hand_2, player_aces, dealer_card_1, deck_number) == true){
							System.out.println("Double it on split!");
							int extra_card;
							extra_card = cards.dealCard(in_hand);
							hand_2 = getPlayerValue(extra_card, hand_2, player_aces);
							split_2.add(extra_card);
							System.out.println(extra_card + "--" + hand_2);
							break;
						}
						while (strat.hitCheck(hand_2, dealer_card_1, player_aces) == true){
							System.out.println("Hit it on a split!");
							int extra_card;
							extra_card = cards.dealCard(in_hand);
							if (aceCheck(extra_card) == true) player_aces++;
							hand_2 = getPlayerValue(extra_card, hand_2, player_aces);
							split_2.add(extra_card);
							System.out.println(extra_card + "--" + hand_2);
						}
						
						break;
					
					}
					
					/**
					 * HAND03
					 */
					if (second_split == true){
						player_aces = 0;
						int extra_3 = cards.dealCard(in_hand);
						if (aceCheck(extra_3) == true) player_aces++;
						
						//checking for a second/third split
						if (extra_3 == player_card_2){
							my_splits++;
							second_split = true;
							System.out.println("First extra: " + extra_3);
							extra_3 = cards.dealCard(in_hand);
							if (aceCheck(extra_3) == true) player_aces++;
							System.out.println("Second split!");
							
							if (extra_3 == player_card_1){
								my_splits++;
								third_split = true;
								System.out.println("Second extra: " + extra_3);
								extra_3 = cards.dealCard(in_hand);
								if (aceCheck(extra_3) == true) player_aces++;
								System.out.println("Third split!");
								
							}
							
						}

						hand_3 = player_card_2;
						
						split_3.add(player_card_2);
						split_3.add(extra_3);
						
						hand_3 = getPlayerValue(extra_3, hand_3, player_aces);
						System.out.println("Another split: \n" + split_3);
						System.out.println(hand_3);
						
						if (hand_3 == 21){
							System.out.println("21 on split!");
						}
						
						//checking hand03
						HAND03LOOP:
						while (true){
							if (strat.doubleCheck(hand_3, player_aces, dealer_card_1, deck_number) == true){
								System.out.println("Double it on split!");
								int extra_card;
								extra_card = cards.dealCard(in_hand);
								hand_3 = getPlayerValue(extra_card, hand_3, player_aces);
								split_3.add(extra_card);
								System.out.println(extra_card + "--" + hand_3);
								break;
							}
							while (strat.hitCheck(hand_3, dealer_card_1, player_aces) == true){
								System.out.println("Hit it on a split!");
								int extra_card;
								extra_card = cards.dealCard(in_hand);
								if (aceCheck(extra_card) == true) player_aces++;
								hand_3 = getPlayerValue(extra_card, hand_3, player_aces);
								split_3.add(extra_card);
								System.out.println(extra_card + "--" + hand_3);
							}
							
							break;
						
						}
					}
					
					/**
					 * 
					 * HAND04
					 * 
					 */
					if (third_split == true){
						player_aces = 0;
						int extra_4 = cards.dealCard(in_hand);
						if (aceCheck(extra_4) == true) player_aces++;

						hand_4 = player_card_2;
						
						split_4.add(player_card_2);
						split_4.add(extra_4);
						
						//System.out.println(extra_2 + " " + hand_2 + " " + player_aces);
						hand_4 = getPlayerValue(extra_4, hand_4, player_aces);
						System.out.println("Another split: \n" + split_4);
						System.out.println(hand_4);
						
						if (hand_4 == 21){
							System.out.println("21 on split!");
						}
						
						//checking hand04
						HAND04LOOP:
						while (true){
							if (strat.doubleCheck(hand_4, player_aces, dealer_card_1, deck_number) == true){
								System.out.println("Double it on split!");
								int extra_card;
								extra_card = cards.dealCard(in_hand);
								hand_4 = getPlayerValue(extra_card, hand_4, player_aces);
								split_4.add(extra_card);
								System.out.println(extra_card + "--" + hand_4);
								break;
							}
							while (strat.hitCheck(hand_4, dealer_card_1, player_aces) == true){
								System.out.println("Hit it on a split!");
								int extra_card;
								extra_card = cards.dealCard(in_hand);
								if (aceCheck(extra_card) == true) player_aces++;
								hand_4 = getPlayerValue(extra_card, hand_4, player_aces);
								split_4.add(extra_card);
								System.out.println(extra_card + "--" + hand_4);
							}
							
							break;
						
						}
					}
					//end of third split
					
				 break;
				}
				//end of split check!
				
				/**
				 * 
				 * 
				 * <><><><><><><><><><><><><><><><><><>
				 * IF THERE IS NO SPLIT YOU START HERE!!!
				 * /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
				 * 
				 * 
				 */
				//double check
				else if (strat.doubleCheck(player_value, player_aces, 
							dealer_card_1, deck_number) == true){
					
					System.out.println("Double it!");
					int extra_card;
					extra_card = cards.dealCard(in_hand);
					player_value = getPlayerValue(extra_card, player_value, player_aces);
					playerList.add(extra_card);
					my_doubles++;
//					bet = (bet * 2);
//					doubles++;
					System.out.println(extra_card + "--" + player_value);
					break;
				}
				
				//hit check
				while (strat.hitCheck(player_value, dealer_card_1, player_aces) == true){
					System.out.println("You hit it!");
					int extra_card;
					extra_card = cards.dealCard(in_hand);
					if (aceCheck(extra_card) == true) player_aces++;
					player_value = getPlayerValue(extra_card, player_value, player_aces);
					playerList.add(extra_card);
					
					System.out.println(extra_card + "--" + player_value);
				}
				
				break;
			}
			//^^^ end of the while(true) loop ^^^
			
			
			if (player_value >= 22){
				System.out.println("You bust! :(");
				player_bust = true;
			}
			
			//dealer strategy
			while (player_bust == false && dealer_hit == true && 
						(dealer_value < 17 || (dealer_value == 17 && dealer_aces > 0))){

				int extra = cards.dealCard(in_hand);
				if (aceCheck(extra)) dealer_aces++;
				dealer_value = getDealerValue(extra, dealer_value, dealer_aces);
				dealerList.add(extra);
				System.out.println("\nDealer hit:\n" + extra + "--" + dealer_value);
				if (dealer_value >= 22){
					dealer_bust = true;
					System.out.println("Dealer busts! :)");
				}
				
			}
			
			System.out.println("----------");
			System.out.println("Checking Scores!");
			System.out.println("Player value: " + player_value);
			System.out.println("Dealer value: " + dealer_value);
			
			hands++;
			
			//player doubles
			double orginal_bet = bet;
			if (my_doubles > 0){
				for (int i = 0; i < my_doubles; i++){
					total_doubles = my_doubles;
					bet += orginal_bet;
					System.out.println("Bet after double: " + bet);
				}
			}
			//dealer blackjack
			if (dealerBJ == true && playerBJ == false){
				my_stack -= bet;
				losses++;
				bet = (bet * 2);
			}
			//player blackjack
			else if (playerBJ == true && dealerBJ == false){
				my_stack += (bet * 1.5);
				wins++;
				bet = table_min;
			}
			//player splits
			else if (my_splits > 0){
				//hand01
				if (hand_1 > dealer_value){
					my_stack += bet;
					split_wins++;
				}
				else if (hand_1 < dealer_value){
					my_stack -= bet;
					split_losses++;
				}
				else if (hand_1 == dealer_value){
					pushes++;
				}
				//hand02
				if (hand_2 > dealer_value){
					my_stack += bet;
					split_wins++;
				}
				else if (hand_2 < dealer_value){
					my_stack -= bet;
					split_losses++;
				}
				else if (hand_2 == dealer_value){
					pushes++;
				}
				//hand03
				if (hand_3 != 0 && (hand_3 > dealer_value)){
					my_stack += bet;
					split_wins++;
				}
				else if (hand_3 != 0 && (hand_3 < dealer_value)){
					my_stack -= bet;
					split_losses++;
				}
				else if (hand_3 != 0 && (hand_3 == dealer_value)){
					pushes++;
				}
				//hand04
				if (hand_4 != 0 && (hand_4 > dealer_value)){
					my_stack += bet;
					split_wins++;
				}
				else if (hand_4 != 0 && (hand_4 < dealer_value)){
					my_stack -= bet;
					split_losses++;
				}
				else if (hand_4 != 0 && (hand_3 == dealer_value)){
					pushes++;
				}
				
				if (split_wins > split_losses) bet = table_min;
				else if (split_losses > split_wins) bet = (split_losses * bet);
				System.out.println("Bet after Splits: " + bet);
			}
			//player bust
			else if (player_bust == true){
				my_stack -= bet;
				bet = (bet * 2);
				losses++;
			}
			//player win
			else if (player_value > dealer_value || dealer_bust == true){
				my_stack += bet;
				bet = table_min;
				wins++;
			}
			//player loss
			else if (player_value < dealer_value){
				my_stack -= bet;
				bet = (bet * 2);
				losses++;
			}
			//push
			else if (player_value == dealer_value){
				pushes++;
				bet = orginal_bet;
			}
			
			System.out.println("--");
			System.out.println("My stack : " + my_stack + " Bet:  " + bet);
			System.out.println("------------");
			dealerList.clear();
			playerList.clear();
			cards.shuffleCheck();
			
		}
		//end of my_stack while loop
		
			System.out.println("------------");
			System.out.println("My stack : " + my_stack + " Bet:  " + bet);
		
	}
	//end of playBJ()
	
	
	/**
	 * returns whether or not card is an ace
	 * @param card
	 * @return
	 */
	public static boolean aceCheck(int card){
		 if (card == 1) return true;
		 return false;
	}
	
	/**
	 * checks and computes players value including aces
	 * @param card
	 * @param total
	 * @param aces
	 * @return
	 */
	public static int getPlayerValue(int card, int total, int aces){
		
		int value = 0;
		value = total + card;
		
		if (card == 1){
			value = total + 11;
			
			if (value > 21){
				value = total + 1;
				player_aces --;
			}
		}
		
		if (value > 21 && aces > 0){
			value = value - 10;
			player_aces --;
		}
		
		return value;

	}

	/**
	 * checks and computes dealers value including aces
	 * @param card
	 * @param total
	 * @param aces
	 * @return
	 */
	public static int getDealerValue(int card, int total, int aces){
		
		int value = 0;
		value = total + card;
		
		if (card == 1){
			value = total + 11;
			
			if (value > 21){
				value = total + 1;
				dealer_aces --;
			}
		}
		
		if (value > 21 && aces > 0){
			value = value - 10;
			dealer_aces --;
		}
		
		return value;
	}
}

