import java.util.Collections;
import java.util.ArrayList;
/**
 * @author daleinen
 * a class representing either
 * 2-4-6-8 decks
 */
public class Deck {

	private ArrayList<Integer> gameDeck = new ArrayList<Integer>();
	private int num_cards;
	private int cards_used;
	private boolean shuffle_later;


		public void gameDeck(int num_decks){
			
			for (int t = 0; t < (num_decks * 16); t++){
				
				this.gameDeck.add(10);
			}
			
			for (int d = 0; d < (num_decks * 4); d++){
				
				for (int c = 1; c <= 9; c++){
					
					this.gameDeck.add(c);
				}
			}
			this.num_cards = num_decks * 52;
			this.cards_used = 1;
		}

        public void deckShuffle(){
        	
            for (int i = 0; i < 3; i++){
            	Collections.shuffle(gameDeck);
            	System.out.println("SHUFFLE!");
            }
            cards_used = 1;

        }
        
        public int dealCard(boolean in_hand){
        	
        	//simulates the cut card at 75% penetration
        	if (cards_used == ((double)num_cards * .75) && in_hand == true) {
        		shuffle_later = true;
        	}
        	if (cards_used == ((double)num_cards * .75) && in_hand == false){
        		this.deckShuffle();
        	}
        	this.cards_used++;
        	return this.gameDeck.get(cards_used - 1);
        }
        
        public void shuffleCheck(){
        	if (this.shuffle_later == true){
        		this.deckShuffle();
        		this.shuffle_later = false;
        	}
        }
        public void printDeck(int numToPrint){

        	// print the top cards
        	int counter = 0;
        	for (int c = 0; c < numToPrint; c++) {
        		System.out.println(this.gameDeck.get(c) + " \t@index " + c);
        		counter++;
        	}
        	System.out.println("\n" + counter + " cards displayed\n");
        }
        	
}

