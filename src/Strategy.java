
public class Strategy 
/*
 * this class uses a custom strategy. dealer_card is only the dealers up card.
 * player_total is both players cards added.
 */
{
	//checks strategy to hit/stand---true is hit, false is stand
	public boolean hitCheck(int player_total, int dealer_card, int aces){
		
		if (player_total >= 4 && player_total <= 11) return true;
			
		else if (player_total == 12 && (dealer_card == 2 || dealer_card == 3)) return true;
		
		else if (player_total == 12 && (dealer_card >= 7 || dealer_card == 1)) return true;
			
		else if ((player_total >= 13 && player_total <= 16) &&
						(dealer_card >= 7 || dealer_card == 1)) return true;
		
		
		if (player_total <= 17 && aces > 0) return true;
		
		else if (player_total == 18 && aces > 0 && 
					(dealer_card == 10 || dealer_card == 9 || dealer_card == 1)) return true;
		
		else return false;

	}
	
	//double check---true is double, false if no double
	public boolean doubleCheck(int player_total, int aces, int dealer_card, int deck_num){
		
		//System.out.println("***IN Double METHOD " + player_total + " " + dealer_card + " " + aces);
		//one or two decks and can only double on 11 and 10
		if (deck_num <= 2){
			
			if (player_total == 11) return true;
			
			else if ((player_total == 10 && aces == 0) && (dealer_card <= 9 && dealer_card != 1)) return true;
		
			else return false;
		}
		
		//4 or more decks
		else if (deck_num >= 4){
			
			if (player_total == 11) return true;
			
			else if (player_total == 10 && dealer_card <= 9 && dealer_card != 1 && aces > 0) return true;
			
			else if (player_total == 9 && dealer_card >= 3 && dealer_card <= 6 && aces > 0) return true;
			
			else if (player_total == 19 && aces > 0 && dealer_card == 6) return true;
			
			else if (player_total == 18 && aces > 0 && dealer_card <= 6 && dealer_card != 1) return true;
			
			else if (player_total == 17 && aces > 0 && dealer_card <= 6 && 
						dealer_card >= 3 && dealer_card != 1) return true;
			
			else if (player_total == 16 && aces > 0 && dealer_card <= 6 && 
						dealer_card >= 4 && dealer_card != 1) return true;
			
			else if ( player_total == 15 && aces > 0 && dealer_card <= 6 
						&& dealer_card >= 4 && dealer_card != 1) return true;
		
			else if (player_total == 14 && aces > 0 && (dealer_card == 6 || dealer_card == 5) 
						&& dealer_card != 1) return true;
			
			else if (player_total == 13 && aces > 0 && (dealer_card == 6 || dealer_card == 5) 
					&& dealer_card != 1) return true;
			
		}
		
		return false;
	}
	
	//check for splits---true is split, false if no split
	public boolean splitCheck(int player_card_1, int player_card_2, int dealer_card, int deck_num){
		
		//one or two decks
		if (deck_num <= 2){
			
			if ((player_card_1 == 1 && player_card_2 == 1) ||
					player_card_1 == 8 && player_card_2 == 8) return true;
			
			else if ((player_card_1 == 9 && player_card_2 == 9) &&
						(dealer_card <= 5 && dealer_card == 7) && dealer_card != 1) return true;
			
			else if ((player_card_1 == 7 && player_card_2 == 7) && 
						(dealer_card <= 8) && dealer_card != 1) return true;
			
			else if ((player_card_1 == 6 && player_card_2 == 6) &&
						(dealer_card <= 7) && dealer_card != 1) return true;
			
			else if ((player_card_1 == 4 && player_card_2 == 4) &&
						(dealer_card == 5 || dealer_card == 6) && dealer_card != 1) return true;
			
			else if (((player_card_1 == 3 && player_card_2 == 3) ||
						(player_card_1 == 2 && player_card_2 == 2)) &&
							(dealer_card <= 7) && dealer_card != 1) return true;
			
		}
		
		//four or more decks
		else if (deck_num >= 4){
			
			if ((player_card_1 == 1 && player_card_2 == 1) ||
					(player_card_1 == 8 && player_card_2 == 8)) return true;
			
			else if ((player_card_1 == 9 && player_card_2 == 9) &&
						(dealer_card <= 5 && dealer_card == 7) && dealer_card != 1) return true;
			
			else if ((player_card_1 == 7 && player_card_2 == 7) && 
						dealer_card <= 7 && dealer_card != 1) return true;
			
			else if ((player_card_1 == 6 && player_card_2 == 6) &&
						dealer_card <= 6 && dealer_card != 1) return true;
			
			else if ((player_card_1 == 4 && player_card_2 == 4) &&
						(dealer_card == 5 || dealer_card == 6) && dealer_card != 1) return true;
			
			else if (((player_card_1 == 3 && player_card_2 == 3) ||
						(player_card_1 == 2 && player_card_2 == 2)) &&
							(dealer_card <= 7) && dealer_card != 1) return true;
			
		}
		
		return false;
	}
	
}
